import models.Time;

public class App {
    public static void main(String[] args) throws Exception {
        Time time1 = new Time(22, 59, 59);

        Time time2 = new Time(23, 59, 59);

        System.out.println("Time 1: " + time1.toString());
        System.out.println("Tang Second 1 len 1s: " + time1.nextSecond());
        System.out.println("Giam Second 1 xuong 1s: " + time1.previousSecond());

        System.out.println("Time 2: " + time2.toString());
        System.out.println("Tang Second 2 len 1s: " + time2.nextSecond());
        System.out.println("Giam Second 2 xuong 1s: " + time2.previousSecond());

    }
}
